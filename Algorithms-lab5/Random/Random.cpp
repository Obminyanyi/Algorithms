#include "stdafx.h"
#include <time.h>
#include <stdio.h>
#include <windows.h>
#include <math.h>
#include <conio.h>
#include <stdlib.h>
#define D 10000

int Generate(int *arr, int n)
{

	for (int i = 0; i < n; i++)
	{
		arr[i] = rand() % 1000;
	}
	return *arr;
}

int Sort1(int *arr, int n)
{
	int tmp, is;
	do {
		is = 0;
		for (int i = 0; i < n - 1; i++)
			if (arr[i] > arr[i + 1])
			{
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				is = 1;
			}
	} while (is);
	return *arr;
}
int Sort2(int *arr, int n)
{
	int imin, a;
	for (int i = 0; i < n - 1; i++)
	{
		imin = i;
		for (int j = i + 1; j < n; j++)
			if (arr[j] < arr[imin]) imin = j;
		a = arr[i];
		arr[i] = arr[imin];
		arr[imin] = a;
	}
	return *arr;
}
int Sort3(int *arr, int n)
{
	for (int i = 1; i < n; i++)
	{
		int c = arr[i];
		for (int j = i - 1; j >= 0 && arr[j] > c; j--)
		{
			arr[j + 1] = arr[j];    arr[j] = c;
		}
	}
	return *arr;
}
int Sort4(int *arr, int n)
{
	int step = n / 2;
	while (step > 0)
	{
		for (int i = 0; i < (n - step); i++)
		{
			int j = i;
			while (j >= 0 && arr[j] > arr[j + step])
			{
				int c = arr[j];
				arr[j] = arr[j + step];
				arr[j + step] = c;
				j--;
			}
		}
		step = step / 2;
	}
	return *arr;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	time((0));
	int num, menu;
	int iter;
	while (1)
	{
		printf("ʳ������ �������� -> ");
		scanf_s("%d", &iter);
		printf("1-����������� ����������\n2-����������� �������\n3-���������� ���������\n4-���������� ������� �����\n������� ���� �������� ����������� ����� -->");
		scanf_s("%d", &num);

		switch (num)
		{
		case 1:
		{
			float dd1, dd2, dd;
			clock_t start2, finish2, start1, finish1;
			{
				SetConsoleCP(1251);
				SetConsoleOutputCP(1251);
				time((0));
				int arr[D] = { 0 }, i = 0, n;
				printf("������ ������� �������� ������:");
				scanf_s("%d", &n);
				start1 = clock(); // ��� ������� ����������� �������� 
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
				}
				finish1 = clock(); //��� ����������  ����������� ��������  
				start2 = clock(); // ��� ������� ���������� ��������  	
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
					Sort1(arr, n);
				}
				finish2 = clock(); //��� ���������� ���������� �������� 	
				printf("\n����������:\n");
				for (int i = 0; i < n; i++)
				{
					printf("%d", arr[i]);
					if (i != n - 1)
						printf("\n");
				}
				printf("\n");
				dd1 = (float)(finish1 - start1);  //��� ���������
				dd2 = (float)(finish2 - start2);  //��� ����������
				dd = (dd2 - dd1) / iter;
				printf("��� ��������� = %f\n", dd);
			}
		}
		break;

		case 2:
		{
			float dd1, dd2, dd;
			clock_t start2, finish2, start1, finish1;
			{
				SetConsoleCP(1251);
				SetConsoleOutputCP(1251);
				time((0));
				int arr[D] = { 0 }, i = 0, n;
				printf("������ ������� �������� ������:");
				scanf_s("%d", &n);
				start1 = clock(); // ��� ������� ����������� �������� 
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
				}
				finish1 = clock(); //��� ���������� ����������� ��������  
				start2 = clock(); // ��� ������� ���������� �������� 
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
					Sort2(arr, n);
				}
				finish2 = clock(); //��� ���������� ���������� �������� 	
				printf("\n����������:\n");
				for (int i = 0; i < n; i++)
				{
					printf("%d", arr[i]);
					if (i != n - 1)
						printf("\n");
				}
				printf("\n");
				dd1 = (float)(finish1 - start1);  //��� ���������
				dd2 = (float)(finish2 - start2);  //��� ����������
				dd = (dd2 - dd1) / iter;
				printf("��� ��������� = %f\n", dd);
			}
		}
		break;


		case 3:
		{
			float dd1, dd2, dd;
			clock_t start2, finish2, start1, finish1;
			{
				SetConsoleCP(1251);
				SetConsoleOutputCP(1251);
				time((0));
				int arr[D] = { 0 }, i = 0, n;
				printf("������ ������� �������� ������:");
				scanf_s("%d", &n);
				start1 = clock(); // ��� ������� ����������� �������� 
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
				}
				finish1 = clock(); //��� ���������� ����������� ��������  
				start2 = clock(); // ��� ������� ���������� �������� 	
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
					Sort3(arr, n);
				}
				finish2 = clock(); //��� ���������� ���������� �������� 	
				printf("\n����������:\n");
				for (int i = 0; i < n; i++)
				{
					printf("%d", arr[i]);
					if (i != n - 1)
						printf("\n");
				}
				printf("\n");
				dd1 = (float)(finish1 - start1);  //��� ���������
				dd2 = (float)(finish2 - start2);  //��� ����������
				dd = (dd2 - dd1) / iter;
				printf("��� ��������� = %f\n", dd);
			}
		}
		break;

		case 4:
		{
			float dd1, dd2, dd;
			clock_t start2, finish2, start1, finish1;
			{
				SetConsoleCP(1251);
				SetConsoleOutputCP(1251);
				time((0));
				int arr[D] = { 0 }, i = 0, n;
				printf("������ ������� �������� ������:");
				scanf_s("%d", &n);
				start1 = clock(); // ��� ������� ����������� �������� 
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
				}
				finish1 = clock(); //��� ���������� ����������� �������� 
				start2 = clock(); // ��� ������� ���������� �������� 	
				for (int i = 0; i < iter; i++)
				{
					Generate(arr, n);
					Sort2(arr, n);
				}
				finish2 = clock(); //��� ���������� ���������� �������� 	
				printf("\n����������:\n");
				for (int i = 0; i < n; i++)
				{
					printf("%d", arr[i]);
					if (i != n - 1)
						printf("\n");
				}
				printf("\n");
				dd1 = (float)(finish1 - start1);  //��� ���������
				dd2 = (float)(finish2 - start2);  //��� ����������
				dd = (dd2 - dd1) / iter;
				printf("��� ��������� = %f\n", dd);
			}
		}
		break;

		default:
		{
			printf("�� ���������� ��� ����� ������\n");
		}
		break;
		}

		printf("1 - ���������� ������\n2 - ��������� ������\n");
		printf("-> ");
		scanf_s("%d", &menu);
		if (menu == 1)
		{
			system("cls");
			continue;
		}
		else
			break;
	}
	return 0;
}


