#include "stdafx.h"
#include "clist.h"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>


// �������� ������� ������
cList* createList(void) {
	cList* list = (cList*)malloc(sizeof(cList));
	if (list) {
		list->size = 0;
		list->head = list->tail = NULL;
	}
	return list;
}

// �������� ������
void deleteList(cList *list) {
	cNode* head = list->head;
	cNode* next = NULL;
	while (head) {
		next = head->next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}

// �������� ������ �� �������
bool isEmptyList(cList* list) {
	return ((list->head == NULL) || (list->tail == NULL));
}

// ���������� ������ ���� � ������ ������
int pushFront(cList* list, elemtype* data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-1);
	}
	node->value = *data;
	node->next = list->head;
	node->prev = NULL;

	if (!isEmptyList(list)) {
		list->head->prev = node;
	}
	else {
		list->tail = node;
	}
	list->head = node;

	list->size++;
	return(0);
}

// ���������� ������ ���� � �������� ������
int pushMiddle(cList* list, elemtype* data, int index)
{
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-1);
	}
	node->value = *data;
	cNode *next = getNode(list, index);
	cNode *prev = next->prev;
	node->next = next;
	node->prev = prev;
	prev->next = node;
	next->prev = node;
	return(0);
}
int popMiddle(cList* list, elemtype* data, int index)
{
	cNode* node;
	if (isEmptyList(list)) {
		return(-2);
	}



	return(0);
}
// ���������� ���� �� ������ ������
int popFront(cList* list, elemtype* data) {
	cNode* node;

	if (isEmptyList(list)) {
		return(-2);
	}

	node = list->head;
	list->head = list->head->next;

	if (!isEmptyList(list)) {
		list->head->prev = NULL;
	}
	else {
		list->tail = NULL;
	}

	*data = node->value;
	list->size--;
	free(node);

	return(0);
}

// ���������� ������ ���� � ����� ������
int pushBack(cList* list, elemtype *data) {
	cNode* node = (cNode*)malloc(sizeof(cNode));
	if (!node) {
		return(-3);
	}

	node->value = *data;
	node->next = NULL;
	node->prev = list->tail;
	if (!isEmptyList(list)) {
		list->tail->next = node;
	}
	else {
		list->head = node;
	}
	list->tail = node;

	list->size++;
	return(0);
}

// ���������� ���� �� ����� ������
int popBack(cList* list, elemtype* data) {
	cNode *node = NULL;

	if (isEmptyList(list)) {
		return(-4);
	}

	node = list->tail;
	list->tail = list->tail->prev;
	if (!isEmptyList(list)) {
		list->tail->next = NULL;
	}
	else {
		list->head = NULL;
	}

	*data = node->value;
	list->size--;
	free(node);

	return(0);
}

// ������ ������������� ���� ������
cNode* getNode(cList* list, int index) {
	cNode *node = NULL;
	int i;

	if (index >= list->size) {
		return (NULL);
	}

	if (index < list->size / 2) {
		i = 0;
		node = list->head;
		while (node && i < index) {
			node = node->next;
			i++;
		}
	}
	else {
		i = list->size - 1;
		node = list->tail;
		while (node && i > index) {
			node = node->prev;
			i--;
		}
	}

	return node;
}

// ����� ������ � �������
void printList(cList* list, void(*func)(elemtype*)) {
	cNode* node = list->head;

	if (isEmptyList(list)) {
		return;
	}

	while (node) {
		func(&(node->value));
		node = node->next;
	}
}
void printNode(elemtype* value) {
	printf("%d\n", *((int*)value));
}

int main() {
	elemtype a;
	elemtype b;
	elemtype c;
	elemtype tmp;
	int index;

	int num;

	setlocale(LC_ALL, "Rus");
	cList* mylist = createList();
	printList(mylist, printNode);
	do
	{
		printf("------------------------\n");
		printf("�������� ��������:\n1 - ���������� ���� � ������\n2 - ���������� ���� �� ������ ������\n3 - ���������� ���� � ����� ������\n");
		printf("4 - ���������� ���� �� ����� ������\n5 - �������� ������\n6 - ����� ������ � �������\n7 - ������ ������������� ���� ������\n0 - �����\n��� ����� ->");
		scanf_s("%d", &num);
		switch (num)
		{

		case 1:
			printf("������� �������\n");
			scanf_s("%d", &a);
			pushFront(mylist, &a);
			break;
		case 2: popFront(mylist, &tmp);
			break;
		case 3:
			printf("������� �������\n");
			scanf_s("%d", &c);
			pushBack(mylist, &c);
			break;
		case 4:	popBack(mylist, &tmp);
			break;
		case 5: deleteList(mylist);
			break;
		case 6:printList(mylist, printNode);
			break;
		case 7:
			cNode* val;
			printf("index=");
			scanf_s("%d", &index);
			val = getNode(mylist, index);
			printf("%d", val);
			break;
		case 8:
			printf("index=");
			scanf_s("%d", &index);
			printf("b=");
			scanf_s("%d", &b);
			pushMiddle(mylist, &b, index);
			break;

		case 0:
			exit(0);
		}

	} while (num != 0);

	return 0;
}
