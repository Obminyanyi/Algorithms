#include "stdafx.h"
#include <malloc.h>
#include <locale.h>
#define count 100
struct stack {
	int elem[count];
	int top;
};
//������������� �����
void init(struct stack *stk) {
	stk->top = 0;
}
//��������� �������� � ����
void push(struct stack *stk, int f) {
	if (stk->top < count) {
		stk->elem[stk->top] = f;
		stk->top++;
	}
	else
		printf("���� ��������, ���������� ���������: %d !\n", stk->top);
}

//�������� �������� �� �����
int pop(struct stack *stk) {
	int elem;
	if ((stk->top) > 0) {
		stk->top--;
		elem = stk->elem[stk->top];
		return(elem);
	}
	else {
		printf("���� ������!\n");
		return(0);
	}
}
//���������� ������� �����
int stkTop(struct stack *stk) {
	if ((stk->top) > 0) {
		return(stk->elem[stk->top - 1]);
	}
	else {
		printf("���� ������!\n");
		return(0);
	}
}
//��������� �������� �������� ����� ��� ��� ��������
int gettop(struct stack *stk) {
	return(stk->top);
}
//�������� ����� �� �������
int isempty(struct stack *stk) {
	if ((stk->top) == 0)    return(1);
	else return(0);
}
//����� ��������� �����
void stkPrint(struct stack *stk) {
	int i;
	i = stk->top;
	if (isempty(stk) == 1) return;
	do {
		i--;
		printf("%d\n", stk->elem[i]);
	} while (i>0);
}
int main()
{
	setlocale(LC_ALL, "Rus");
	struct stack *stk;
	int i, n;
	int elem;

	stk = (struct stack*)malloc(sizeof(struct stack));
	init(stk);
	printf("������� ���������� ��������� � �����: ");
	scanf_s("%d", &n);
	for (i = 0; i<n; i++) {
		printf("������� ������� %d -> ", i);
		scanf_s("%d", &elem);
		push(stk, elem);
	}
	printf("� ����� %d ���������\n", gettop(stk));
	stkPrint(stk);
	printf("������� ������� %d\n", stkTop(stk));
	do {
		printf("��������� ������� %d, ", pop(stk));
		printf("� ����� �������� %d ���������\n", gettop(stk));
	} while (isempty(stk) == 0);

	return 0;
}
