#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <malloc.h>
#include <locale.h>
#define MAX 40


typedef struct Node {
	int value;
	struct Node *next;
} Node;
Node *head = NULL;
//������� ����� ����
void push(Node **head, int data) {
	Node *tmp = (Node*)malloc(sizeof(Node));
	tmp->value = data;
	tmp->next = (*head);
	(*head) = tmp;
}
//������� ������ ������� � ���������� ���
int pop(Node **head) {
	Node* prev = NULL;
	int val;
	if (head == NULL) {
		exit(-1);
	}
	prev = (*head);
	val = prev->value;
	(*head) = (*head)->next;
	free(prev);
	return val;
}
//
Node* getNth(Node* head, int n) {
	int counter = 0;
	while (counter < n && head) {
		head = head->next;
		counter++;
	}
	return head;
	// ��������� ���������� �� �������
}Node* getLast(Node *head) {
	if (head == NULL) {
		return NULL;
	}
	while (head->next) {
		head = head->next;
	}
	return head;
}
//��������� ����� �������
void pushBack(Node *head, int value) {
	Node *last = getLast(head);
	Node *tmp = (Node*)malloc(sizeof(Node));
	tmp->value = value;
	tmp->next = NULL;
	last->next = tmp;
}
//������� ��������
void insert(Node *head, unsigned n, int val) {
	unsigned i = 0;
	Node *tmp = NULL;
	//���� ������� ������� ��� �����	

	while (i < n && head->next) {
		head = head->next;
		i++;
	}
	tmp = (Node*)malloc(sizeof(Node));
	tmp->value = val;
	//���� ��� �� ��������� �������, �� next ������������ �� ��������� ����
	if (head->next) {
		tmp->next = head->next;
		//����� �� NULL
	}
	else {
		tmp->next = NULL;
	}
	head->next = tmp;
}
//�������� �������� ������
int deleteNth(Node **head, int n) {
	if (n == 0) {
		return pop(head);
	}
	else {
		Node *prev = getNth(*head, n - 1);
		Node *elm = prev->next;
		int val = elm->value;

		prev->next = elm->next;
		free(elm);
		return val;
	}
}
//������� ���� ������ 
int deleteList(Node **head) {
	Node* prev = NULL;
	while ((*head)->next) {
		prev = (*head);
		(*head) = (*head)->next;
		free(prev);
	}
	free(*head);
	return 0;
}
//������� ��������� �� �����
void printLinkedList(const Node *head) {
	while (head) {
		printf("%d ", head->value);
		head = head->next;
	}
	printf("\n");
}
//������� ������ �� ������
void fromArray(Node **head, int *arr, size_t size) {
	size_t i = size - 1;
	if (arr == NULL || size == 0) {
		return;
	}
	do {
		push(head, arr[i]);
	} while (i-- != 0);
}
int main()
{
	setlocale(LC_ALL, "Rus");
	Node* head = NULL;
	int x;
	int n;
	int arr[MAX];
	int index, value;

	do
	{
		printf("-----------------\n");
		printf("�������� ��������:\n1 - ���������� ������ ���� �� ��������\n2 - ���������� ���� �� ��������\n3 - ���������� ������ ���� � ����� ������\n");
		printf("4 - �������� ������\n5 - ����� ������ � �������\n6 - �������� ������\n0 - ���i�\n��� ����� -> ");
		scanf_s("%d", &x);
		switch (x)
		{
		case 1:
			printf("������� ������: ");
			scanf_s("%d", &index);
			printf("������� �����: ");
			scanf_s("%d", &value);
			insert(head, index, value);
			break;
		case 2:printf("������� ������ ����� ������� ������ �������: ");
			scanf_s("%d", &index);
			deleteNth(&head, index);
			break;
		case 3:
			printf("������� ����� ��� ���������� � ����� ������: ");
			scanf_s("%d", &value);
			pushBack(head, value);
			break;
		case 4:deleteList(&head);
			break;
		case 5:printLinkedList(head);
			break;
		case 6:
			int n;
			printf("������� ���������� �����: \n");
			scanf_s("%d", &n);
			printf("������� ����: \n");
			for (int i = 0; i < n; i++)
				scanf_s("%d", &arr[i]);
			fromArray(&head, arr, n);
			break;
		case 0:exit(0);

		}

	} while (x != 0);

	return 0;
}

