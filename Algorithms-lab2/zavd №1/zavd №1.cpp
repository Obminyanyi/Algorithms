#include "stdafx.h"
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>


#define lenth 1000 // ����� ������������������ �����
#define Diapazon 100 // �������� ��������� �����

int main()
{

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));

	long long m = pow(2, 31);
	long long a = 48271;
	long long arr[lenth];
	int c = 0;
	int *frequency;
	frequency = (int*)calloc((Diapazon + 1), sizeof(int));

	double P[Diapazon + 1]; // ����������� ����������� ��������� �����
	arr[0] = time(NULL); // ������� ����� � ��������

	for (int i = 0; i < lenth - 1; i++)
	{
		arr[i + 1] = ((long long)(a*arr[i] + c)) % m;
	}

	int arr2[lenth];
	for (int i = 0; i <lenth; i++)

	{
		arr2[i] = (double)arr[i] / (m - 1) * 201; // ��������� �������� � ���������� [0; 100] 
	}

	for (int i = 0; i < Diapazon + 1; i++)
	{
		for (int j = 0; j < lenth; j++)
		{
			if (arr2[j] == i)
				frequency[i]++;
		}
		printf("������� %d: %d\n", i, frequency[i]);
		P[i] = (double)(frequency[i]) / 1000;
		printf("����������� %d: %f\n", i, P[i]);
	}
	printf("---------------------------------\n");

	double expectation = 0; //����������� ���������
	for (int i = 0; i <= Diapazon; i++)
		expectation += i * P[i];
	printf("����������� ��������� = %f\n", expectation);

	double dispersion = 0; //�������� ����������� �����
	for (int i = 0; i <= Diapazon; i++)
		dispersion += pow((i - expectation), 2) * P[i];
	printf("�������� = %f\n", dispersion);


	double standardDeviation = 0; //������������������� (����������) ���������  
	for (int i = 0; i <= Diapazon; i++)
		standardDeviation = sqrt(dispersion);
	printf("������������������� ��������� = %f\n", standardDeviation);


	return 0;
}
