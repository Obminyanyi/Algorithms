#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;

double func_y(double x)
{
	return x*x*x / 3.0;
}

double Simpson(double a, double b, double dx)
{
	if (a>b)
	{
		return 0;
	}
	double S = 0, x, h;

	a = 0;
	b = 1;

	h = (b - a) / 10000;
	x = a + h;
	while (x < b)
	{
		S = S + 4 * func_y(x);
		x = x + h;

		if (x >= b) break;
		S = S + 2 * func_y(x);
		x = x + h;
	}
	S = (h / 3)*(S + func_y(a) + func_y(b));

	return S;
}

void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	double S = Simpson(0, 1, 0.001);
	printf("�������� = %f\n", S);
	_getch();
}
