#include "stdafx.h"
#include <math.h>
#include <time.h>
#include <stdlib.h>
double func(double x)
{
	return x*x*x / 3.0;
}
int main()
{
	int a = 0;
	int b = 1;
	int number = 1000000;
	double s = 0;
	double x;
	double y;

	srand((unsigned)time(NULL));
	for (int i = 0; i<number; i++)
	{
		x = (double)(rand()) / RAND_MAX*1.0;
		y = (double)(rand()) / RAND_MAX*1.0;

		if (fabs(y) < func(x))
			s += 1;
	}

	s = s / (double)number*(b - a);
	printf("Integral = %f \n", s);

	return 0;
}
