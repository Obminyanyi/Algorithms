#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;

double func_y(double x)
{
	return x*x*x / 3.0;
}

double Trap_Solve(double a, double b, double dx)
{
	if (a>b)
	{
		return 0;
	}
	double x = a;
	double S = 0.;
	while (x + dx <= b)
	{
		S += dx*(func_y(x) + func_y(x + dx)) / 2.;
		x += dx;
	}
	return S;
}

void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	double S = Trap_Solve(0, 1, 0.001);
	printf("�������� = %f\n", S);
	_getch();
}
