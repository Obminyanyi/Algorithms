#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;

double func_y(double x)
{
	return x*x*x / 3.0;
}

double Pryamoygolnik(double a, double b, double dx)
{
	if (a>b)
	{
		return 0;
	}
	double x = a;
	double S = 0.;

	while (x <= b)
	{
		S = dx*(func_y(x + dx / 2.0)) + S;
		x = dx + x;
	}
	return S;
}


void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	double S = Pryamoygolnik(0, 1, 0.001);
	printf("�������� = %f\n", S);
	_getch();
}
