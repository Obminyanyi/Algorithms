#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
void main()
{
	signed char ex1, ex2, ex3, ex5, ex6;
	unsigned char ex4;
	ex1 = 5 + 127;
	ex2 = 2 - 3;
	ex3 = -120 - 34;
	ex4 = (unsigned char)(-5);
	ex5 = 56 & 38;
	ex6 = 56 | 38;

	printf("1) 5 + 127 = %d\n", ex1);
	printf("2) 2 - 3 = %d\n", ex2);
	printf("3) -120 - 34 = %d\n", ex3);
	printf("4)(unsigned char)(-5) = %d\n", ex4);
	printf("5)56 & 38 = %d\n", ex5);
	printf("6)56 | 38 = %d\n", ex6);
	_getch();
}

