#include "stdafx.h"
#include "windows.h"
union float32 {
	float value;
	struct Pobit {
		unsigned char b0 : 1;
		unsigned char b1 : 1;
		unsigned char b2 : 1;
		unsigned char b3 : 1;
		unsigned char b4 : 1;
		unsigned char b5 : 1;
		unsigned char b6 : 1;
		unsigned char b7 : 1;
		unsigned char b8 : 1;
		unsigned char b9 : 1;
		unsigned char b10 : 1;
		unsigned char b11 : 1;
		unsigned char b12 : 1;
		unsigned char b13 : 1;
		unsigned char b14 : 1;
		unsigned char b15 : 1;
		unsigned char b16 : 1;
		unsigned char b17 : 1;
		unsigned char b18 : 1;
		unsigned char b19 : 1;
		unsigned char b20 : 1;
		unsigned char b21 : 1;
		unsigned char b22 : 1;
		unsigned char b23 : 1;
		unsigned char b24 : 1;
		unsigned char b25 : 1;
		unsigned char b26 : 1;
		unsigned char b27 : 1;
		unsigned char b28 : 1;
		unsigned char b29 : 1;
		unsigned char b30 : 1;
		unsigned char b31 : 1;
	}bit;
} a;
void printBytes(float32 a);
void printCharacters(float32 a);
void printBits(float32 a);
int _tmain(int argc, _TCHAR* argv[])
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("������ ������� �����: ");
	float ab;
	scanf_s("%f", &a.value);
	printBits(a);
	printBytes(a);
	printCharacters(a);
	printf("��������� ����� %d �����\n", sizeof(a.bit));
	return 0;
}
void printBytes(float32 a)
{
	printf("2) �������� ��������:\n1 ���� %d%d%d%d%d%d%d%d\n2 ���� %d%d%d%d%d%d%d%d\n3 ���� %d%d%d%d%d%d%d%d\n4 ���� %d%d%d%d%d%d%d%d\n", a.bit.b7,
		a.bit.b6, a.bit.b5, a.bit.b4, a.bit.b3, a.bit.b2,
		a.bit.b1, a.bit.b0, a.bit.b15, a.bit.b14,
		a.bit.b13, a.bit.b12, a.bit.b11, a.bit.b10, a.bit.b9,
		a.bit.b8, a.bit.b23, a.bit.b22, a.bit.b21, a.bit.b20,
		a.bit.b19, a.bit.b18, a.bit.b17, a.bit.b16, a.bit.b31,
		a.bit.b30, a.bit.b29, a.bit.b28, a.bit.b27, a.bit.b26,
		a.bit.b25, a.bit.b24);
}
void printBits(float32 a)
{
	printf("1) �������� �������: %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n", a.bit.b31,
		a.bit.b30, a.bit.b29, a.bit.b28, a.bit.b27, a.bit.b26,
		a.bit.b25, a.bit.b24, a.bit.b23, a.bit.b22, a.bit.b21, a.bit.b20,
		a.bit.b19, a.bit.b18, a.bit.b17, a.bit.b16, a.bit.b15, a.bit.b14,
		a.bit.b13, a.bit.b12, a.bit.b11, a.bit.b10, a.bit.b9, a.bit.b8,
		a.bit.b7, a.bit.b6, a.bit.b5, a.bit.b4, a.bit.b3, a.bit.b2,
		a.bit.b1, a.bit.b0);
}
void printCharacters(float32 a)
{
	char *z = (char*)malloc(22);
	if (a.bit.b31 == 1)
		z = "(����� ��'����)";
	if (a.bit.b31 == 0)
		z = "(����� �������)";
	if (a.bit.b31 == 0 && a.value == 0)
		z = "(����� ����)";
	printf("3) ���� %d %s\n �������������� %d%d%d%d%d%d%d%d\n ������������� ������� %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n", a.bit.b31, z,
		a.bit.b30, a.bit.b29, a.bit.b28, a.bit.b27, a.bit.b26,
		a.bit.b25, a.bit.b24, a.bit.b23, a.bit.b22, a.bit.b21, a.bit.b20,
		a.bit.b19, a.bit.b18, a.bit.b17, a.bit.b16, a.bit.b15, a.bit.b14,
		a.bit.b13, a.bit.b12, a.bit.b11, a.bit.b10, a.bit.b9, a.bit.b8,
		a.bit.b7, a.bit.b6, a.bit.b5, a.bit.b4, a.bit.b3, a.bit.b2,
		a.bit.b1, a.bit.b0);
}
